# Makefile for s6-boot

VERSION = $$(git describe --tags| sed 's/-.*//g;s/^v//;')
PKGNAME = s6-boot

BINDIR_EXECLINE = /usr/local/bin
BINDIR = /usr/bin

CLASSIC = `find classic/ -type f`
DATA = `find data/scripts -type f`
RC = `find rc/ -type f`
FILESYSTEM = `find filesystem-env/ -type f`
ENV = `find env/ -type f`

DIRS= \
	/usr/bin \
	/etc/s6 \
	/etc/s6/compiled \
	/etc/s6/data/scripts \
	/etc/s6/env \
	/etc/s6/filesystem-env \
	/etc/s6/rc \
	/etc/s6/classic/service \
	/etc/s6/classic/uncaught-logs \
	/usr/share/zsh/site-functions \
	/usr/share/licenses

install:
	install -dm755 $(addprefix $(DESTDIR),$(DIRS))
	
	for i in $(CLASSIC) $(RC) init stage2 stage2.tini stage3 poweroff reboot ; do \
		sed -i 's,@BINDIR_EXECLINE@,$(BINDIR_EXECLINE),' $$i; \
	done 
	
	for i in $(DATA) shutdown $(CLASSIC); do \
		sed -i 's,@BINDIR@,$(BINDIR),' $$i; \
	done
		
	for i in $(CLASSIC); do \
		install -Dm755 $$i $(DESTDIR)/etc/s6/$$i; \
	done
	
	for i in $(RC); do \
		install -Dm644 $$i $(DESTDIR)/etc/s6/$$i; \
	done
	
	for i in $(FILESYSTEM); do \
		install -Dm644 $$i $(DESTDIR)/etc/s6/$$i;\
	done
	
	for i in $(ENV); do \
		install -m644 $$i $(DESTDIR)/etc/s6/$$i; \
	done
	
	rm $(DESTDIR)/etc/s6/classic/uncaught-logs/README
	
	install -m755 data/scripts/s6.local $(DESTDIR)/etc/s6/data/scripts/s6.local
	ln -sf /etc/s6/data/scripts/s6.local $(DESTDIR)/etc/s6.local
	install -m755 data/scripts/master.sh $(DESTDIR)/etc/s6/data/scripts/master.sh
	install -m755 data/scripts/modules.sh $(DESTDIR)/etc/s6/data/scripts/modules.sh
	install -m755 data/scripts/tmpfiles.sh $(DESTDIR)/etc/s6/data/scripts/tmpfiles.sh
	
	cp -P -a compiled/default $(DESTDIR)/etc/s6/compiled/default
	ln -sf /etc/s6/compiled/default $(DESTDIR)/etc/s6/compiled/current
	ln -sf /etc/s6/compiled/default $(DESTDIR)/etc/s6/compiled/previous
		
	install -m755 init $(DESTDIR)/$(BINDIR)
	for i in stage2 stage2.tini stage3 reboot shutdown poweroff; do \
		install -m755 $$i $(DESTDIR)/etc/s6/$$i;\
	done
	ln -sf /etc/s6/poweroff $(DESTDIR)/$(BINDIR)
	ln -sf /etc/s6/reboot $(DESTDIR)/$(BINDIR)
	ln -sf /etc/s6/shutdown $(DESTDIR)/$(BINDIR)
	
	install -m644 s6.conf $(DESTDIR)/etc/s6
	ln -sf /etc/s6/s6.conf $(DESTDIR)/etc/s6.conf
	
	install -m644 _s6-svc $(DESTDIR)/usr/share/zsh/site-functions
	
	install -Dm644 LICENSE $(DESTDIR)/usr/share/licenses/$(PKGNAME)/LICENSE
	
version:
	@echo $(VERSION)
	
.PHONY: install version
